<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class getRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getRate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get rate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $start = microtime(true);

        $url = env('PROVIDER');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        //Convert  json to array
        $results = json_decode($result, true);

        //Find right value
        foreach ($results as $key => $result) {
            if ($key == 'USD') {
                $USD = $result;
                foreach ($USD as $key => $value) {
                    ${'USD' . $key} = $value;
                }
            }
            if ($key == 'EUR') {
                $EUR = $result;
                foreach ($EUR as $key => $value) {
                    ${'EUR' . $key} = $value;
                }
            }
            if ($key == 'GBP') {
                $GBP = $result;
                foreach ($GBP as $key => $value) {
                    ${'GBP' . $key} = $value;
                }
            }

        }

        $date = Carbon::now();
        $end = microtime(true);
        $runtime = $end - $start;
        //        insert table
        DB::insert("insert into rates (15m_usd,last_usd,buy_usd,sell_usd,symbol_usd,15m_eur,last_eur,buy_eur,sell_eur,symbol_eur,15m_gbp,last_gbp,buy_gbp,sell_gbp,symbol_gbp,created_at,updated_for ) values ('$USD15m','$USDlast','$USDbuy','$USDsell','$USDsymbol','$EUR15m','$EURlast','$EURbuy','$EURsell','$EURsymbol','$GBP15m','$GBPlast','$GBPbuy','$GBPsell','$GBPsymbol','$date','$runtime')", [1, 'Dayle']);


        Log::info('Command start: ' . $date);
        Log::info('Name provider: ' . $url);
        Log::info('Runtime command getRate: ' . $runtime);

        dd('Success, runtime ' . ($end - $start));
    }
}
