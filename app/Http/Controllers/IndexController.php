<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Rate;

class IndexController extends Controller
{
    public function index() {
        $rates = DB::table('rates')->paginate(20);
        return view('rate', ['rates' => $rates]);
    }
}
