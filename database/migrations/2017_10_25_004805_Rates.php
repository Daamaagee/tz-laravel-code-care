<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function ($table) {
            $table->increments('id');
            $table->float('15m_usd');
            $table->float('last_usd');
            $table->float('buy_usd');
            $table->float('sell_usd');
            $table->string('symbol_usd');
            $table->float('15m_eur');
            $table->float('last_eur');
            $table->float('buy_eur');
            $table->float('sell_eur');
            $table->string('symbol_eur');
            $table->float('15m_gbp');
            $table->float('last_gbp');
            $table->float('buy_gbp');
            $table->float('sell_gbp');
            $table->string('symbol_gbp');
            $table->timestamp('created_at');
            $table->float('updated_for');
        });;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
