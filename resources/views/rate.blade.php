<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!-- Styles -->
    <style>
        .table, th {
            text-align: center;
        }

        .grey {
            background: #e8e8e8;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="container">
        <div class="row">
            <table class="table">
                <thead class="thead-inverse">
                <tr>
                    <th>TIME</th>
                    <th colspan="2" class="grey">USD</th>
                    <th colspan="2">EUR</th>
                    <th colspan="2" class="grey">GBP</th>
                </tr>
                </thead>
                <thead class="thead-inverse">
                <tr>
                    <th></th>
                    <th class="grey">buy</th>
                    <th class="grey">sell</th>
                    <th>buy</th>
                    <th>sell</th>
                    <th class="grey">buy</th>
                    <th class="grey">sell</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($rates as $rate)
                    <tr>
                        <th scope="row">{{ $rate->created_at }}</th>
                        <td class="grey">{{ $rate->buy_usd }} {{ $rate->symbol_usd }}</td>
                        <td class="grey">{{ $rate->sell_usd }} {{ $rate->symbol_usd }}</td>
                        <td>{{ $rate->buy_eur }} {{ $rate->symbol_eur }}</td>
                        <td>{{ $rate->sell_eur }} {{ $rate->symbol_eur }}</td>
                        <td class="grey">{{ $rate->buy_gbp }} {{ $rate->symbol_gbp }}</td>
                        <td class="grey">{{ $rate->sell_gbp }} {{ $rate->symbol_gbp }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row text-center">
            {{ $rates->links() }}
        </div>
    </div>
</div>
</body>
</html>
